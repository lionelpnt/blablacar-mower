package com.blablacar.projectmower.application.utils;

import com.blablacar.projectmower.application.exception.IncorrectInstructionException;
import com.blablacar.projectmower.application.exception.IncorrectMaximalCoordinatesException;
import com.blablacar.projectmower.application.exception.IncorrectFileException;
import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.Mower;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class util for reading a input file.
 *
 * @author Lionel
 */
public class FileUtils {

    private FileUtils() {

    }

    /**
     * Retrieve all mowers from an input file.
     *
     * @param path
     * @return list of mowers
     * @throws IncorrectInstructionException
     * @throws IncorrectFileException
     */
    public static List<Mower> getMowerList(Path path) throws IncorrectInstructionException,
            IncorrectFileException {
        List<String> mowerList;
        List<String> instructionList;
        List<Mower> mowers;

        try (Stream<String> stream = Files.lines(path)) {
            mowerList = stream
                    .filter(ligne -> ligne.matches("([\\d]+ [\\d]+ (N|S|E|W))$"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IncorrectFileException(e.getMessage());
        }

        try (Stream<String> stream = Files.lines(path)) {
            instructionList = stream
                    .filter(ligne -> ligne.matches("(F|R|L)+$"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IncorrectFileException(e.getMessage());
        }

        if (mowerList.size() != instructionList.size()) {
            throw new IncorrectInstructionException("Instructions missing or incorrects");
        }

        mowers = new ArrayList<>();

        for (int i = 0; i < mowerList.size(); i++) {
            Mower mower = new Mower();
            mower.setCoordinatesAndOrientation(mowerList.get(i));
            mower.addInstructions(instructionList.get(i));
            mowers.add(mower);
        }

        return mowers;
    }

    /**
     * Get coordinates max from input file.
     *
     * @param path
     * @return coordinates
     * @throws IncorrectMaximalCoordinatesException
     * @throws IncorrectFileException
     */
    public static Coordinates getGroundMaxCoordinates(Path path) throws IncorrectMaximalCoordinatesException, IncorrectFileException {
        final Optional<String> coordinatesMax;
        try (Stream<String> stream = Files.lines(path)) {
            coordinatesMax = stream
                    .filter(ligne -> ligne.matches("([\\d]+ [\\d]+)$"))
                    .findFirst();

            return coordinatesMax.
                    map(FileUtils::retrieveCoordinatesMax)
                    .orElseThrow(IncorrectMaximalCoordinatesException::new);
        } catch (IOException e) {
            throw new IncorrectFileException(e.getMessage());
        }
    }

    /**
     * Retrieve coordinates max
     *
     * @param line
     * @return Coordinates
     */
    private static Coordinates retrieveCoordinatesMax(String line) {
        String[] inputs = line.split(" ");
        Coordinates coordinates = new Coordinates();

        coordinates.setX(Integer.valueOf(inputs[0]));
        coordinates.setY(Integer.valueOf(inputs[1]));

        return coordinates;
    }
}
