package com.blablacar.projectmower.application.model;

/**
 * Enum that represent all instructions
 *
 * @author Lionel
 */

public enum Instruction {
    /**
     * Instruction Forward.
     */
    F(0),

    /**
     * Instruction Left.
     */
    L(-1),

    /**
     * Instruction Right.
     */
    R(1);

    private Integer value;

    Instruction(Integer value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
