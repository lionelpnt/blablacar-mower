package com.blablacar.projectmower.application.model;

/**
 * Enum that represent application's parameters.
 * 
 * @author Lionel
 *
 */
public enum ParametersName {

	/**
	 * Source
	 */
	F_SOURCE("input");

	private String name;

	private ParametersName(String name) {
		this.name = name;
	}

	/**
	 * @return enum value
	 */
	public String getValue() {
		return name;
	}
}
