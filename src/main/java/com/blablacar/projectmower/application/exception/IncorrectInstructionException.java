package com.blablacar.projectmower.application.exception;

/**
 * Exception for incorrect format instructions line. 
 * 
 * @author Lionel
 *
 */
public class IncorrectInstructionException extends Exception {

	private static final long serialVersionUID = -1061787670537136485L;

	public IncorrectInstructionException(String message) {
		super(message);
	}

}
