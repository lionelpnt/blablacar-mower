package com.blablacar.projectmower.application.exception;

/**
 * 
 * Exception for incorrect input file.
 * 
 * @author Lionel
 * 
 */
public class IncorrectFileException extends Exception {

	private static final long serialVersionUID = -57375607283356254L;

	public IncorrectFileException(String message) {
		super(message);
	}
}
