package com.blablacar.projectmower.application.exception;

/**
 * Exception for maximals coordinates line. 
 * 
 * @author Lionel
 *
 */
public class IncorrectMaximalCoordinatesException extends Exception {

	private static final long serialVersionUID = 6599871631161432028L;

	public IncorrectMaximalCoordinatesException() {
		super();
	}
}
