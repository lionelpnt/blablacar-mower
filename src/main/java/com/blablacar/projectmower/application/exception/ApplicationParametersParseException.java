/**
 * 
 */
package com.blablacar.projectmower.application.exception;

/**
 * 
 * Exceptions for invalids application's parameters.
 * 
 * @author Lionel
 *
 */
public class ApplicationParametersParseException extends Exception {


  private static final long serialVersionUID = -4295341812170236181L;

  public ApplicationParametersParseException() {
  }

  public ApplicationParametersParseException(String message) {
    super(message);
  }

  public ApplicationParametersParseException(Throwable cause) {
    super(cause);
  }

}
