package com.blablacar.projectmower.application;

import com.blablacar.projectmower.application.exception.*;
import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.ExitStatus;
import com.blablacar.projectmower.application.model.Mower;
import com.blablacar.projectmower.application.service.MowerService;
import com.blablacar.projectmower.application.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

/**
 * @author Lionel
 */
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            run(args);
        } catch (ApplicationTerminateException e) {
            System.exit(e.getExitStatus().getValue());
        }
    }

    private static void run(String[] args) {
        final ApplicationParameters applicationParameters = new ApplicationParameters();

        try {
            applicationParameters.parse(args);
        } catch (ApplicationParametersParseException e) {
            logError(e);
            terminateApplication(ExitStatus.EX_USAGE);
        }

        final Path path = applicationParameters.getPath();
        try {
            final MowerService mowerService = new MowerService();

            final Coordinates coordinatesMax = FileUtils.getGroundMaxCoordinates(path);
            List<Mower> mowers = mowerService.getMowersFromFile(path);
            mowerService.realizeInstructionsAll(mowers, coordinatesMax);

        } catch (IncorrectInstructionException | IncorrectFileException | IncorrectMaximalCoordinatesException e) {
            logError(e);
            terminateApplication(ExitStatus.EX_RUNEX);
        }

    }

    private static void terminateApplication(ExitStatus exitStatus) {
        throw new ApplicationTerminateException(exitStatus);
    }

    private static void logError(Exception e) {
        String exMessage = e.getMessage();
        if (e.getCause() != null) {
            exMessage += " [cause] " + e.getCause().getMessage();
        }
        LOGGER.error(exMessage);
    }
}
