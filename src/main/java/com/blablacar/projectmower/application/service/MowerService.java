package com.blablacar.projectmower.application.service;

import com.blablacar.projectmower.application.exception.IncorrectFileException;
import com.blablacar.projectmower.application.exception.IncorrectInstructionException;
import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.Instruction;
import com.blablacar.projectmower.application.model.Mower;
import com.blablacar.projectmower.application.model.Orientation;
import com.blablacar.projectmower.application.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

/**
 * Service dedicated to mowers.
 *
 * @author Lionel
 */
public class MowerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MowerService.class);

    public MowerService() {

    }

    /**
     * Retrieve all mowers from an input file
     *
     * @param path
     * @return mower's list
     * @throws IncorrectInstructionException
     * @throws IncorrectFileException
     */
    public List<Mower> getMowersFromFile(Path path) throws IncorrectInstructionException, IncorrectFileException {
        List<Mower> mowers = FileUtils.getMowerList(path);
        return mowers;
    }

    /**
     * Realize all mowers instructions from a mower list.
     *
     * @param mowers
     * @param coordinatesMax
     */
    public void realizeInstructionsAll(List<Mower> mowers, Coordinates coordinatesMax) {
        for (Mower mower : mowers) {
            realizeInstructions(coordinatesMax, mower);
        }
    }

    /**
     * Realize all mowers instructions.
     *
     * @param coordinatesMax
     * @param mower
     */
    private void realizeInstructions(Coordinates coordinatesMax, Mower mower) {
        if (mower.getInstructions() != null) {
            for (Instruction instruction : mower.getInstructions()) {
                switch (instruction) {
                    case F:
                        forward(mower.getCoordinates(), mower.getOrientation(), coordinatesMax);
                        break;
                    case R:
                    case L:
                        mower.setOrientation(getNewOrientation(mower.getOrientation(), instruction));
                        break;
                    default:
                        break;
                }
            }
            mower.getInstructions().clear();
        }

        LOGGER.info(mower.getCurrentCoordinatesAndOrientation());
    }

    /**
     * Get the new mower orientation
     *
     * @param orientation
     * @param instruction
     * @return new Orientation
     */
    private Orientation getNewOrientation(Orientation orientation, Instruction instruction) {
        int newOrientationValue = (orientation.getValue() + instruction.getValue());
        newOrientationValue = Math.floorMod(newOrientationValue, 4);

        orientation = Orientation.valueOf(newOrientationValue);

        return orientation;
    }

    /**
     * Realize a forward intruction according to the current mower orientation.
     *
     * @param coordinates
     * @param orientation
     * @param coordinatesMax
     */
    private void forward(Coordinates coordinates, Orientation orientation, Coordinates coordinatesMax) {
        switch (orientation) {

            case N:
                goToNorth(coordinates, coordinatesMax.getY());
                break;
            case E:
                goToEast(coordinates, coordinatesMax.getX());
                break;
            case S:
                goToSouth(coordinates);
                break;
            case W:
                goToWest(coordinates);
                break;
            default:
                break;
        }
    }

    /**
     * Calculates new coordinates after moving to East.
     *
     * @param coordinates
     * @param xMax
     */
    private void goToEast(Coordinates coordinates, int xMax) {
        if (coordinates.getX() != xMax) {
            coordinates.setX(coordinates.getX() + 1);
        }
    }

    /**
     * Calculates new coordinates after moving to West.
     *
     * @param coordinates
     */
    private void goToWest(Coordinates coordinates) {
        if (coordinates.getX() != 0) {
            coordinates.setX(coordinates.getX() - 1);
        }
    }

    /**
     * Calculates new coordinates after moving to the North.
     *
     * @param coordinates
     * @param yMax
     */
    private void goToNorth(Coordinates coordinates, int yMax) {
        if (coordinates.getY() != yMax) {
            coordinates.setY(coordinates.getY() + 1);
        }
    }

    /**
     * Calculates new coordinates after moving to the South.
     *
     * @param coordinates
     */
    private void goToSouth(Coordinates coordinates) {
        if (coordinates.getY() != 0) {
            coordinates.setY(coordinates.getY() - 1);
        }
    }

}
