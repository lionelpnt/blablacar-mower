package com.blablacar.projectmower.application.utils;

import com.blablacar.projectmower.application.SetupTest;
import com.blablacar.projectmower.application.exception.IncorrectInstructionException;
import com.blablacar.projectmower.application.exception.IncorrectMaximalCoordinatesException;
import com.blablacar.projectmower.application.exception.IncorrectFileException;
import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.Mower;
import com.blablacar.projectmower.application.model.Orientation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileUtilsTest extends SetupTest {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test getCoordinatesMax method.
     *
     * @throws IncorrectFileException
     * @throws IncorrectMaximalCoordinatesException
     */
    @Test
    public void getGroundMaxCoordinatesTest() throws IncorrectMaximalCoordinatesException, IncorrectFileException {
        Coordinates coordinatesFromFile = FileUtils.getGroundMaxCoordinates(path);
        assertEquals(coordinatesFromFile, coordinatesMax);
    }

    /**
     * Test getMowerList method.
     *
     * @throws IncorrectInstructionException
     * @throws IncorrectFileException
     */
    @Test
    public void getMowerListTest() throws IncorrectInstructionException, IncorrectFileException {
        List<Mower> mowers = FileUtils.getMowerList(path);

        assertEquals(mowers.size(), 2);
        Mower mower = mowers.get(0);

        assertEquals(Orientation.N, mower.getOrientation());
        assertEquals(new Coordinates(1, 2), mower.getCoordinates());
        assertEquals(setInstructionMower1(), mower.getInstructions());

        mower = mowers.get(1);

        assertEquals(Orientation.E, mower.getOrientation());
        assertEquals(new Coordinates(3, 3), mower.getCoordinates());
        assertEquals(setInstructionMower2(), mower.getInstructions());
    }

}
