package com.blablacar.projectmower.application.service;

import com.blablacar.projectmower.application.SetupTest;
import com.blablacar.projectmower.application.exception.IncorrectFileException;
import com.blablacar.projectmower.application.exception.IncorrectInstructionException;
import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.Mower;
import com.blablacar.projectmower.application.model.Orientation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MowerServiceTest extends SetupTest {

    private MowerService mowerService;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        mowerService = new MowerService();
    }


    /**
     * Test realizeInstructionsAll method.
     */
    @Test
    public void realizeInstructionsAllTest() {
        mowerService.realizeInstructionsAll(mowers, coordinatesMax);

        Mower mower = mowers.get(0);

        assertEquals(new Coordinates(1, 3), mowers.get(0).getCoordinates());
        assertEquals(mower.getOrientation(), Orientation.N);

        mower = mowers.get(1);

        assertEquals(new Coordinates(5, 1), mowers.get(1).getCoordinates());
        assertEquals(mower.getOrientation(), Orientation.E);

    }

    /**
     * Test getMowersFromFile method.
     *
     * @throws IncorrectInstructionException
     * @throws IncorrectFileException
     */
    @Test
    public void getMowersFromFileTest() throws IncorrectInstructionException, IncorrectFileException {
        List<Mower> mowerList = mowerService.getMowersFromFile(path);

        assertEquals(mowerList.size(), 2);
        Mower mower = mowerList.get(0);

        assertEquals(Orientation.N, mower.getOrientation());
        assertEquals(new Coordinates(1, 2), mower.getCoordinates());
        assertEquals(setInstructionMower1(), mower.getInstructions());

        mower = mowerList.get(1);

        assertEquals(Orientation.E, mower.getOrientation());
        assertEquals(new Coordinates(3, 3), mower.getCoordinates());
        assertEquals(setInstructionMower2(), mower.getInstructions());
    }

}
