package com.blablacar.projectmower.application;

import com.blablacar.projectmower.application.model.Coordinates;
import com.blablacar.projectmower.application.model.Instruction;
import com.blablacar.projectmower.application.model.Mower;
import com.blablacar.projectmower.application.model.Orientation;
import org.junit.Before;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SetupTest {

    protected Coordinates coordinatesMax;
    protected List<Mower> mowers;
    protected Path path;

    @Before
    public void setUp() throws Exception {
        mowers = new ArrayList<>();
        setMower1();
        setMower2();
        coordinatesMax = new Coordinates(5, 5);
        path = Paths.get("ressources/test/files/instruc.txt");
    }

    protected List<Instruction> setInstructionMower1() {
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(Instruction.L);
        instructions.add(Instruction.F);
        instructions.add(Instruction.L);
        instructions.add(Instruction.F);
        instructions.add(Instruction.L);
        instructions.add(Instruction.F);
        instructions.add(Instruction.L);
        instructions.add(Instruction.F);
        instructions.add(Instruction.F);

        return instructions;
    }

    protected List<Instruction> setInstructionMower2() {
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(Instruction.F);
        instructions.add(Instruction.F);
        instructions.add(Instruction.R);
        instructions.add(Instruction.F);
        instructions.add(Instruction.F);
        instructions.add(Instruction.R);
        instructions.add(Instruction.F);
        instructions.add(Instruction.R);
        instructions.add(Instruction.R);
        instructions.add(Instruction.F);

        return instructions;
    }

    private void setMower1() {
        Mower mower = new Mower();
        mower.setCoordinates(new Coordinates(1, 2));
        mower.setOrientation(Orientation.N);
        mower.setInstructions(setInstructionMower1());
        mowers.add(mower);
    }

    private void setMower2() {
        Mower mower = new Mower();
        mower.setCoordinates(new Coordinates(3, 3));
        mower.setOrientation(Orientation.E);

        mower.setInstructions(setInstructionMower2());
        mowers.add(mower);
    }

}
