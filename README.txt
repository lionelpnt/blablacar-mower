Le projet est un projet java simple sans utilisation du framework Spring (Boot ou non).
Il se lance avec un fichier à passer en paramètre du programme avec ce qui est nécessaire, comme indiqué sur l'énoncé,
c'est à dire :
- première ligne pour la taille du quadrillage, suivi de coordonnées de tondeuses avec leurs instructions.
Pour indiquer le fichier, il faut l'option "-input" puis l'emplacement du fichier.
Ce qui donne si vous lancez le jar présent à la racine du projet en ligne de commande
java -jar mower.jar -input instruc.txt
Si vous le lancez avec intellij, vous pouvez ajouter un paramètre d'exécution dans "Edit Configuration"